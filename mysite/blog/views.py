from django.shortcuts import render,render_to_response
from django import forms
from django.http import HttpResponse
from .models import user
from django.views.decorators.csrf import csrf_exempt
from PIL import Image
import matplotlib.pyplot as plt
from django.http import JsonResponse

# Create your views here.
class UserForm(forms.Form):
    username = forms.CharField(required=False)
    headImg = forms.ImageField(required=False)
@csrf_exempt
# def demo(request):
#     print(request)
#     if request.method == "POST":
#         print(request.POST)
#         uf = UserForm(request.POST,request.FILES)
#         if uf.is_valid():
#             print(request.FILES)
#             img=Image.open(request.FILES['file'])
#             ########################在这里写调用函数
#             plt.imshow(img)
#             # plt.show()
#             #获取表单信息request.FILES是个字典
#             User = user(headImg=request.FILES['file'])
#             #保存在服务器
#             print(User)
#             User.save()
#             a=2
#             #return HttpResponse('upload ok!')
#             return HttpResponse(a)
#
#     return render(request, 'blog/index.html')


def index(request):
    if request.method == "POST":
        uf = UserForm(request.POST,request.FILES)
        if uf.is_valid():
            try:

                if request.FILES['file']:
                    img = Image.open(request.FILES['file'])
                    ########################在这里写调用函数
                    plt.imshow(img)
                    # plt.show()
                    # 获取表单信息request.FILES是个字典
                    User = user(headImg=request.FILES['file'])
                    # 保存在服务器
                    print(User)
                    User.save()
                    a = 2
                    # return HttpResponse('upload ok!')
                    return HttpResponse(a)

            except Exception as ee:
                if request.POST.get('url'):
                    url = request.POST.get('url')
                    print(url)
                    print("get here")
                    # return render(request, 'blog/index.html')
                    data = {
                        'name': 'zhangsan',
                        'age': 18,
                    }
                    # return JsonResponse(data)
                    print( JsonResponse(data))
                    return JsonResponse(data)
                    # return HttpResponse("url ok")

                if request.FILES.get('root'):
                    obj = request.FILES.get('root')
                    img = Image.open(obj)

                    plt.imshow(img)
                    plt.show()

                if request.FILES.get('video'):
                    obj = request.FILES.get('root')
                    img = Image.open(obj)

                    plt.imshow(img)
                    plt.show()

                if request.POST['path']:
                    path = request.POST['path']
                    print(path)
                    return render(request, 'blog/index.html', {'path': path})


    return render(request, 'blog/index.html')

def upload(request):
    if request.method == 'POST':# 获取对象
        obj = request.FILES.get('root')
        # 获取文件的一个对象
        path = request.POST['path']
        # 获取手工输入的路径
    return render(request, 'upload.html',{'path':path})
    # 把path这个变量，作为参数传递给，upload.html


def login(request):
    return render(request,'login.html')
def reg(request):
    if request.method == 'POST':
        url=request.POST.get('url')
        print(url)
        return HttpResponse("url ok")
    return render(request,'blog/index.html')



# Create your views here.
# def index(request):
#     return render(request, 'blog/index.html')
